Source: pynast
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Andreas Tille <tille@debian.org>,
           Tim Booth <tbooth@ceh.ac.uk>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3,
               python3-cogent3,
               python3-sphinx,
               ncbi-blast+-legacy,
               clustalw,
               mafft,
               muscle
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/med-team/pynast
Vcs-Git: https://salsa.debian.org/med-team/pynast.git
Homepage: https://github.com/biocore/pynast

Package: pynast
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         libjs-jquery,
         libjs-underscore
Recommends: ncbi-blast+-legacy,
            clustalw,
            mafft,
            muscle,
            python3-mpi4py
Suggests: uclust
Conflicts: python-pynast
Provides: python-pynast
Replaces: python-pynast
Description: alignment of short DNA sequences
 The package provices a reimplementation of the Nearest Alignment
 Space Termination tool in Python. It was prepared for next generation
 sequencers.
 .
 Given a set of sequences and a template alignment, PyNAST will align the
 input sequences against the template alignment, and return a multiple
 sequence alignment which contains the same number of positions (or
 columns) as the template alignment. This facilitates the analysis of new
 sequences in the context of existing alignments, and additional data
 derived from existing alignments such as phylogenetic trees. Because
 any protein or nucleic acid sequences and template alignments can be
 provided, PyNAST is not limited to the analysis of 16s rDNA sequences.
